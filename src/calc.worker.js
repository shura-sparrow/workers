import { BubbleSort, randomArray, ARRAY_LENGTH, logger } from './utils';

self.addEventListener('message', () => {
    logger('Начало обработки массива: ', console.warn);
    BubbleSort(randomArray(ARRAY_LENGTH));
    self.postMessage(null);
});

