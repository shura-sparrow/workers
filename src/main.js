import Worker from './calc.worker';
import { 
    logger, 
    BubbleSort, 
    randomArray, 
    ARRAY_LENGTH, 
    NODES_COL 
} from './utils';

const NODES_REPEAT = 20;
function load() {
    if (window.Worker) {
        const worker = new Worker();

        worker.onmessage = function () {
            logger('Конец обработки массива: ', console.warn);
            worker.terminate();
        }

        let radio = document.querySelector('input');

        if (radio.checked) {
            worker.postMessage(null);
        } else {
            logger('Начало обработки массива: ', console.warn);
            BubbleSort(randomArray(ARRAY_LENGTH));
            logger('Конец обработки массива: ', console.warn);
        }

        logger('Начало вставки элементов: ', console.log);
        let i = 0;
        var timerId = setInterval(function() {
            var j = 0;
            while (j < NODES_REPEAT) {
                appendNodes();
                j++;
            }
            if (i == NODES_COL) {
                clearInterval(timerId);
                logger('Конец вставки элементов: ', console.log);
            }
            i += NODES_REPEAT;
        });
    }   
}

window.onload = function() {
   document.getElementById('start').addEventListener('click', load);
}

function appendNodes() {
    const radio = document.createElement('input');
    radio.type = 'radio';
    radio.checked = true;
    document.getElementById('radios').appendChild(radio);
}
