export const ARRAY_LENGTH = 50000;
export const NODES_COL = 10000;

export const getFullTime = (d) => {
    return [d.getHours(), d.getMinutes(), d.getSeconds()].map(function (x) {
        return x < 10 ? "0" + x : x
    }).join(":")
}

export const BubbleSort = (arr) => {
    var n = arr.length;
    for (var i = 0; i < n-1; i++) { 
        for (var j = 0; j < n-1-i; j++) { 
            if (arr[j+1] < arr[j])
            { 
                var t = arr[j+1]; arr[j+1] = arr[j]; arr[j] = t; 
            }
        }
    }                     
    return arr;
}

export const randomArray = (n) => {
    let arr = [];
    for (let i = 0; i < n; i++) {
        arr.push(Math.random(i));
    }
    return arr;
}

export const logger = (message, fn) => {
    fn(message + getFullTime(new Date()));
}