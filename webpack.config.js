let webpack = require('webpack');
let path = require('path');

module.exports = {
    entry: {
        main: path.resolve('./src/main.js')
    },
    module: {
        rules: [
            {
                test: /\.worker\.js$/,
                loader: 'worker-loader',
                options: { 
                    publicPath: './dist/',
                    name: 'calc.worker.js'
                }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
            }
        ]
    },
    output: {
        path: path.join(__dirname, '/dist'),
        filename: '[name].js',
        libraryTarget: 'umd',
    },
    mode: 'development'
}